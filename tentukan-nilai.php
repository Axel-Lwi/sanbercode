<?php

// 85 - 100 Sangat Baik
// 70 - 84 Baik
// 60 - 69 Cukup
// dibawah 59 Kurang

function tentukan_nilai($number)
{
    $ket="";
    if($number <101 && $number >=85){
        $ket = "Sangat Baik";
    } else if($number < 85 && $number >=70){
        $ket = "Baik";
    } else if($number < 70 && $number >=60){
        $ket = "Cukup";
    } else if($number > 100 ){
        $ket = "Nilai salah, Max Nilai 100, Tolong Cek Kembali";
    } else {
        $ket = "Kurang";
    }
    return $ket."<br>";        
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang

//buat test
// echo tentukan_nilai(85); //sb
// echo tentukan_nilai(100); //sb
// echo tentukan_nilai(101); //kurang karena gamungkin nilai diatas 100
// echo tentukan_nilai(70);  //baik
// echo tentukan_nilai(84);  //baik
// echo tentukan_nilai(60);  //cukup
// echo tentukan_nilai(59);  //kurang
?>