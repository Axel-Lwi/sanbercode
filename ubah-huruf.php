<?php
function ubah_huruf($string){
    $huruf = "abcdefghijklmnopqrstuvwxyz";
    $hasil = "";
    for($i = 0; $i < strlen($string); $i++){
        $index = strrpos($huruf, $string[$i]);      // cari tau dia di alfabet huruf ke brp
        $hasil .= substr($huruf, $index +1, 1);    //mengeluarkan substring dari abjad yang indexnya =1 dari string input
    }
    return $hasil."<br>";    
}
// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>